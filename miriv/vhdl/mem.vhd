library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.mem_pkg.all;
use work.op_pkg.all;

entity mem is
	port (
		clk				: in  std_logic;
		reset			: in  std_logic;
		stall			: in  std_logic;
		flush			: in  std_logic;

		-- to Ctrl
		mem_busy		: out std_logic;

		-- from EXEC
		mem_op			: in  mem_op_type;
		wbop_in	  		: in  wb_op_type;
		pc_new_in		: in  pc_type;
		pc_old_in		: in  pc_type;
		aluresult_in	: in  data_type;
		wrdata			: in  data_type;
		zero			: in  std_logic;

		-- to EXEC (forwarding)
		reg_write		: out reg_write_type;

		-- to FETCH
		pc_new_out		: out pc_type;
		pcsrc			: out std_logic;

		-- to WB
		wbop_out		: out wb_op_type;
		pc_old_out		: out pc_type;
		aluresult_out	: out data_type;
		memresult		: out data_type;

		-- memory controller interface
		mem_out	 		: out mem_out_type;
		mem_in			: in  mem_in_type;

		-- exceptions
		exc_load		: out std_logic;
		exc_store		: out std_logic
	);
end mem;

architecture rtl of mem is

	signal int_memop		: mem_op_type;
	signal int_wbop			: wb_op_type;
	signal int_pc_new		: pc_type;
	signal int_pc_old		: pc_type;
	signal int_aluresult_in	: data_type;
	signal int_wrdata		: data_type;
	signal int_zero			: std_logic;
	-- signal int_pcsrc		: std_logic;

	signal int_memu_a		: data_type;

begin

	memu_inst : entity work.memu
	port map(
		-- in
		op	=> int_memop.mem,
		-- A	=> int_memu_a, -- addr
		A	=> int_aluresult_in, -- addr
		W	=> int_wrdata, -- wrdata
		D	=> mem_in,
		-- out
		R	=> memresult,
		B	=> mem_busy,
		XL	=> exc_load,
		XS	=> exc_store,
		M	=> mem_out
	);

	input : process(clk, reset, flush)
	begin
		if reset = '0' then
			int_memop			<= MEM_NOP;
			int_wbop			<= WB_NOP;
			int_pc_new			<= ZERO_PC;
			int_pc_old			<= ZERO_PC;
			int_aluresult_in	<= (others => '0');
			int_wrdata			<= (others => '0');
			int_zero			<= '0';

		elsif rising_edge(clk) then
				if stall ='1' then
					int_memop.mem.memread <= '0';
					int_memop.mem.memwrite <= '0';
				else
					if flush = '0' then
						int_memop			<= mem_op;
						int_wbop			<= wbop_in;
						int_pc_new			<= pc_new_in;
						int_pc_old			<= pc_old_in;
						int_aluresult_in	<= aluresult_in;
						int_wrdata			<= wrdata;
						int_zero			<= zero;
					else
						int_memop			<= MEM_NOP;
						int_wbop			<= WB_NOP;
					end if;
				end if;
		end if;
	end process;

	wbop_out		<= int_wbop;
	pc_old_out		<= int_pc_old;
	pc_new_out		<= int_pc_new;
	aluresult_out	<= int_aluresult_in;

	reg_write.write	<= int_wbop.write;
	reg_write.reg	<= int_wbop.rd;
	-- reg_write.data	<= int_aluresult_in;
	mem : process (all)
	begin
		case int_wbop.src is
			when WBS_ALU =>
				reg_write.data <= int_aluresult_in;
			when WBS_MEM =>
				reg_write.data <= memresult;
			when WBS_OPC =>  -- old PC + 4
				reg_write.data <= to_data_type(std_logic_vector(unsigned(int_pc_old)+x"4"));
		end case;

		-- pcsrc decide if branch
		case int_memop.branch is
			when BR_BR =>
				pcsrc <= '1';
			when BR_CND =>
				pcsrc <= int_zero;
			when BR_CNDI =>
				pcsrc <= not int_zero;
			when BR_NOP =>
				pcsrc <= '0';
		end case;

	end process;

end architecture;
