library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.op_pkg.all;
use work.mem_pkg.all;

entity fetch is
	port (
		clk			: in  std_logic;
		reset		: in  std_logic;
		stall		: in  std_logic;
		flush		: in  std_logic;

		-- to control
		mem_busy	: out std_logic;

		pcsrc		: in  std_logic;
		pc_in		: in  pc_type;
		pc_out		: out pc_type := (others => '0');
		instr		: out instr_type;

		-- memory controller interface
		mem_out		: out mem_out_type;
		mem_in		: in  mem_in_type
	);
end fetch;

architecture rtl of fetch is

	signal int_pc		: pc_type;
	signal int_pc_next	: pc_type;
	signal int_instr	: instr_type;

begin


	mem_busy <= mem_in.busy;

	mem_out <= (
		address	=> int_pc_next(PC_WIDTH-1 downto 2),
		rd		=> MEM_OUT_NOP.rd,
		wr		=> MEM_OUT_NOP.wr,
		byteena	=> MEM_OUT_NOP.byteena,
		wrdata	=> MEM_OUT_NOP.wrdata
	);


	programm_cnt : process(all)
	begin
		int_pc_next	<= int_pc;
		if stall = '0' and reset = '1' then
			if pcsrc = '1' then
				int_pc_next	<= pc_in;
			else
				int_pc_next	<= std_logic_vector(unsigned(int_pc) +
								to_unsigned(4, PC_WIDTH));
			end if;
		end if;
	end process;


	flush_proc : process(all)
	begin
		if flush = '0' and mem_in.rddata /= (mem_data_type'RANGE => 'X')then
			instr(31 downto 24)	<= mem_in.rddata(7 downto 0);
			instr(23 downto 16)	<= mem_in.rddata(15 downto 8);
			instr(15 downto 8)	<= mem_in.rddata(23 downto 16);
			instr(7 downto 0)	<= mem_in.rddata(31 downto 24);
		else
			instr <= NOP_INST;
		end if;
	end process;


	sync_proc : process(clk, reset)
	begin
		if reset = '0' then
			int_pc	<= std_logic_vector(to_signed(-4, PC_WIDTH));
			-- int_pc	<= ZERO_PC;
		elsif rising_edge(clk) then
			int_pc	<= int_pc_next;
			pc_out	<= int_pc_next;
		end if;
	end process;



end architecture;
