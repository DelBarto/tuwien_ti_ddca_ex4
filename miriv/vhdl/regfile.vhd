library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.mem_pkg.all;


entity regfile is
    port (
        clk              : in  std_logic;
        reset            : in  std_logic;
        stall            : in  std_logic;
        rdaddr1, rdaddr2 : in  reg_adr_type;
        rddata1, rddata2 : out data_type;
        wraddr           : in  reg_adr_type;
        wrdata           : in  data_type;
        regwrite         : in  std_logic
    );
end entity;

architecture rtl of regfile is
	type register_t is array(2**REG_BITS-1 downto 0) of std_logic_vector(DATA_WIDTH-1 downto 0);
	signal register_arr : register_t := (others => (others =>'0'));

	signal buf_addr1 : reg_adr_type := (others => '0');
	signal buf_addr2 : reg_adr_type := (others => '0');
begin

	input : process(all)
	begin
		if reset = '0' then
			buf_addr1 <= (others => '0');
			buf_addr2 <= (others => '0');
			register_arr <= (others => (others =>'0'));
		elsif rising_edge(clk) then
			if stall = '0' then
				buf_addr1 <= rdaddr1;
				buf_addr2 <= rdaddr2;
			end if;

			if (regwrite = '1' and to_integer(unsigned(wraddr)) /= 0)  then
				register_arr(to_integer(unsigned(wraddr))) <= wrdata;
			end if;
		end if;
	end process;

    rddata1 <= wrdata when (buf_addr1 = wraddr and regwrite = '1')
			else register_arr(0) when to_integer(unsigned(buf_addr1)) = 0
			else register_arr(to_integer(unsigned(buf_addr1)));

    rddata2 <= wrdata when (buf_addr2 = wraddr and regwrite = '1')
			else register_arr(0) when to_integer(unsigned(buf_addr2)) = 0
			else register_arr(to_integer(unsigned(buf_addr2)));

end architecture;
