library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.op_pkg.all;

entity exec is
	port (
		clk				: in  std_logic;
		reset			: in  std_logic;
		stall			: in  std_logic;
		flush			: in  std_logic;

		-- from DEC
		op				: in  exec_op_type;
		pc_in			: in  pc_type;

		-- to MEM
		pc_old_out		: out pc_type;
		pc_new_out		: out pc_type;
		aluresult		: out data_type;
		wrdata			: out data_type;
		zero			: out std_logic;

		memop_in		: in  mem_op_type;
		memop_out		: out mem_op_type;
		wbop_in			: in  wb_op_type;
		wbop_out		: out wb_op_type;

		-- to ctrl
		exec_op			: out exec_op_type;
		-- to FWD
		reg_write_mem	: in reg_write_type;
		reg_write_wr	: in reg_write_type
	);
end exec;

architecture rtl of exec is


	signal int_A : data_type;
	signal int_B : data_type;
	signal int_R : data_type;
	signal int_Z : std_logic;
	signal int_op : exec_op_type;
	signal int_pc_in : pc_type;
	signal int_memop_in : mem_op_type;
	signal int_wbop_in : wb_op_type;
	-- FWD 
	signal val1 : data_type;
	signal val2 : data_type;
	signal do_fwd1 : std_logic;
	signal do_fwd2 : std_logic;

begin

	alu_inst : entity work.alu
	port map(
		op => int_op.aluop, --alu_op_type
		A => int_A,
		B => int_B,
		R => int_R,
		Z => int_Z
	);

		fwd1_inst : entity work.fwd
		port map(
			-- from Mem
			reg_write_mem	=> reg_write_mem,

			-- from WB
			reg_write_wb	=> reg_write_wr,

			-- from/to EXEC
			reg				=> int_op.rs1, -- reg_adr_type
			val				=> val1, --out
			do_fwd			=> do_fwd1 --out
		);

		fwd2_inst : entity work.fwd
		port map(
			-- from Mem
			reg_write_mem	=> reg_write_mem,

			-- from WB
			reg_write_wb	=> reg_write_wr,

			-- from/to EXEC
			reg				=> int_op.rs2, -- reg_adr_type
			val				=> val2, --out
			do_fwd			=> do_fwd2 --out
		);


	input : process(clk, reset, flush, stall)
	begin
		if reset = '0' then
			int_pc_in		<= (others => '0');
			int_op			<= EXEC_NOP;
			int_memop_in	<= MEM_NOP;
			int_wbop_in		<= WB_NOP;
		elsif rising_edge(clk) then
		 	if stall = '0' then
				if flush = '0' then
					int_pc_in		<= pc_in;
					int_op			<= op;
					int_memop_in	<= memop_in;
					int_wbop_in		<= wbop_in;
				else
					-- int_pc_in		<= (others => '0');
					int_op			<= EXEC_NOP;
					int_memop_in	<= MEM_NOP;
					int_wbop_in		<= WB_NOP;
				end if;
			end if;
		end if;
	end process;


	multiplex : process(all)
		variable rddata1 : data_type;
		variable rddata2 : data_type;
	begin

		exec_op		<= int_op;
		pc_old_out	<= int_pc_in;
		memop_out	<= int_memop_in;
		wbop_out	<= int_wbop_in;

		-- handle forwarding for rs1
		if (do_fwd1 = '1') then
			rddata1 := val1;
		else
			rddata1 := int_op.readdata1;
		end if;

		-- handle forwarding for rs2
		if (do_fwd2 = '1') then
			rddata2 := val2;
		else
			rddata2 := int_op.readdata2;
		end if;


		int_A <= (others => '0');
		int_B <= (others => '0');

		if int_op.alusrc1 = '1' then
			int_A <= rddata1;
		else
			if int_op.alusrc3 = '1' then
				int_A <= to_data_type(int_pc_in);
			end if;
		end if;


		if int_op.alusrc2 = '0' or
			int_memop_in.mem.memread = '1' or
			int_memop_in.mem.memwrite = '1' then
				int_B <= int_op.imm;
		else
			int_B <= rddata2;
		end if;



		-- calc pc_new
		pc_new_out <= int_pc_in;
		if int_memop_in.branch = BR_BR then
			if int_op.alusrc1 = '1' then
				pc_new_out <= to_pc_type(std_logic_vector(unsigned(int_op.imm)
								+ unsigned(rddata1)));
				pc_new_out(0) <= '0';
			else
				pc_new_out <= to_pc_type(std_logic_vector(unsigned(int_pc_in)
							+ unsigned(int_op.imm)));
			end if;

		elsif int_memop_in.branch = BR_CND or int_memop_in.branch = BR_CNDI  then
			pc_new_out <= to_pc_type(std_logic_vector(unsigned(int_pc_in)
							+ unsigned(int_op.imm)));
		end if;

		aluresult <= int_R;
		zero <= int_Z;
		wrdata <= rddata2;


	end process;

end architecture;
