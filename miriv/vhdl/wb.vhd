library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.core_pkg.all;
use work.op_pkg.all;

entity wb is
	port (
		clk			: in  std_logic;
		reset		: in  std_logic;
		stall		: in  std_logic;
		flush		: in  std_logic;

		-- from MEM
		op			: in  wb_op_type;
		aluresult	: in  data_type;
		memresult	: in  data_type;
		pc_old_in	: in  pc_type;

		-- to FWD and DEC
		reg_write	: out reg_write_type
	);
end wb;

architecture rtl of wb is

	signal int_wbop			: wb_op_type;
	signal int_aluresult	: data_type;
	signal int_memresult	: data_type;
	signal int_pc_old		: pc_type;

begin

	input : process(clk, reset, flush, stall)
	begin
		if reset = '0' then
			int_wbop		<= WB_NOP;
			int_aluresult	<= (others => '0');
			int_memresult	<= (others => '0');
			int_pc_old		<= (others => '0');
		elsif rising_edge(clk) then
			if stall = '0' then
				if flush = '0' then
				int_wbop		<= op;
				int_aluresult	<= aluresult;
				int_memresult	<= memresult;
				int_pc_old		<= pc_old_in;

				else
					int_wbop		<= WB_NOP;
				end if;
			end if;
		end if;
	end process;

	wb : process (all)
	begin

		reg_write.reg	<= int_wbop.rd;
		reg_write.write	<= int_wbop.write;

		case int_wbop.src is
			when WBS_ALU =>
				reg_write.data <= int_aluresult;
			when WBS_MEM =>
				reg_write.data <= int_memresult;
			when WBS_OPC =>  -- old PC + 4
				reg_write.data <= to_data_type(std_logic_vector(unsigned(int_pc_old)+x"4"));
		end case;

	end process;

end architecture;
