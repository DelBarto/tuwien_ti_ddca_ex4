library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mem_pkg.all;
use work.cache_pkg.all;
use work.single_clock_rw_ram_pkg.all;

entity data_st_1w is
	generic (
		SETS_LD  : natural := SETS_LD
	);
	port (
		clk	   : in std_logic;
		we		: in std_logic;
		rd		: in std_logic;
		index	 : in c_index_type;
		byteena   : in mem_byteena_type;
		data_in   : in mem_data_type;
		data_out  : out mem_data_type
);
end entity;

architecture impl of data_st_1w is

	alias data_in_0 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_in(DATA_WIDTH-1 downto 3*BYTE_WIDTH);
	alias data_in_1 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_in(3*BYTE_WIDTH-1 downto 2*BYTE_WIDTH);
	alias data_in_2 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_in(2*BYTE_WIDTH-1 downto BYTE_WIDTH);
	alias data_in_3 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_in(BYTE_WIDTH-1 downto 0);

	signal data_in_ram_0 : std_logic_vector(BYTE_WIDTH-1 downto 0);
	signal data_in_ram_1 : std_logic_vector(BYTE_WIDTH-1 downto 0);
	signal data_in_ram_2 : std_logic_vector(BYTE_WIDTH-1 downto 0);
	signal data_in_ram_3 : std_logic_vector(BYTE_WIDTH-1 downto 0);

	alias byteena_0 : std_logic is byteena(3);
	alias byteena_1 : std_logic is byteena(2);
	alias byteena_2 : std_logic is byteena(1);
	alias byteena_3 : std_logic is byteena(0);

	alias data_out_0 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_out(DATA_WIDTH-1 downto 3*BYTE_WIDTH);
	alias data_out_1 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_out(3*BYTE_WIDTH-1 downto 2*BYTE_WIDTH);
	alias data_out_2 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_out(2*BYTE_WIDTH-1 downto BYTE_WIDTH);
	alias data_out_3 : std_logic_vector(BYTE_WIDTH-1 downto 0) is data_out(BYTE_WIDTH-1 downto 0);

	signal scram_out_0 : std_logic_vector(BYTE_WIDTH-1 downto 0);
	signal scram_out_1 : std_logic_vector(BYTE_WIDTH-1 downto 0);
	signal scram_out_2 : std_logic_vector(BYTE_WIDTH-1 downto 0);
	signal scram_out_3 : std_logic_vector(BYTE_WIDTH-1 downto 0);

	signal we_0 : std_logic;
	signal we_1 : std_logic;
	signal we_2 : std_logic;
	signal we_3 : std_logic;

begin
		data_in_ram_0 <= data_in_0	when byteena_0 = '1' else (others => '0');
		data_in_ram_1 <= data_in_1	when byteena_1 = '1' else (others => '0');
		data_in_ram_2 <= data_in_2	when byteena_2 = '1' else (others => '0');
		data_in_ram_3 <= data_in_3	when byteena_3 = '1' else (others => '0');

		data_out_0 <= scram_out_0;
		data_out_1 <= scram_out_1;
		data_out_2 <= scram_out_2;
		data_out_3 <= scram_out_3;

		we_0 <= byteena_0 and we;
		we_1 <= byteena_1 and we;
		we_2 <= byteena_2 and we;
		we_3 <= byteena_3 and we;


	single_clock_rw_ram_0_inst : single_clock_rw_ram
		generic map (
			ADDR_WIDTH => INDEX_SIZE,
			DATA_WIDTH => BYTE_WIDTH
		)
		port map (
			clk		   => clk,
			data_in	   => data_in_ram_0,
			write_address => index,
			read_address  => index,
			we			=> we_0,
			data_out	  => scram_out_0
		);

	single_clock_rw_ram_1_inst : single_clock_rw_ram
		generic map (
			ADDR_WIDTH => INDEX_SIZE,
			DATA_WIDTH => BYTE_WIDTH
		)
		port map (
			clk		   => clk,
			data_in	   => data_in_ram_1,
			write_address => index,
			read_address  => index,
			we			=> we_1,
			data_out	  => scram_out_1
		);

	single_clock_rw_ram_2_inst : single_clock_rw_ram
		generic map (
			ADDR_WIDTH => INDEX_SIZE,
			DATA_WIDTH => BYTE_WIDTH
		)
		port map (
			clk		   => clk,
			data_in	   => data_in_ram_2,
			write_address => index,
			read_address  => index,
			we			=> we_2,
			data_out	  => scram_out_2
		);

	single_clock_rw_ram_3_inst : single_clock_rw_ram
		generic map (
			ADDR_WIDTH => INDEX_SIZE,
			DATA_WIDTH => BYTE_WIDTH
		)
		port map (
			clk		   => clk,
			data_in	   => data_in_ram_3,
			write_address => index,
			read_address  => index,
			we			=> we_3,
			data_out	  => scram_out_3
		);

end architecture;
