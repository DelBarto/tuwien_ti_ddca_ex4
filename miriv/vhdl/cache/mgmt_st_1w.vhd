library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mem_pkg.all;
use work.cache_pkg.all;

entity mgmt_st_1w is
    generic (
        SETS_LD  : natural := SETS_LD
    );
    port (
        clk     : in std_logic;
        reset   : in std_logic;

        index   : in c_index_type;
        we      : in std_logic;
        -- we_repl : in std_logic;

        mgmt_info_in  : in c_mgmt_info;
        mgmt_info_out : out c_mgmt_info
);
end entity;

architecture impl of mgmt_st_1w is
    type mgmt_infos_t is array(0 to 2**SETS_LD-1) of c_mgmt_info;
    constant empty_mgmt_info : c_mgmt_info := (valid => '0',
												dirty => '0',
												-- replace => '0',
												tag => (others=> '0'));
    signal mgmt_infos : mgmt_infos_t := (others => empty_mgmt_info);
begin

	mgmt_info_out <= mgmt_infos(to_integer(unsigned(index)));

    process(clk, reset)
    begin
        if reset = '0' then
            mgmt_infos <= (others => empty_mgmt_info);
        elsif rising_edge(clk) then
            if we = '1' then
                mgmt_infos(to_integer(unsigned(index))) <= mgmt_info_in;
            end if;
        end if;
    end process;


end architecture;
