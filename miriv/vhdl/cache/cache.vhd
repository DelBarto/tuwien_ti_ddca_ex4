library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.mem_pkg.all;
use work.cache_pkg.all;

entity cache is
	generic (
		SETS_LD   : natural		  := SETS_LD;
		-- WAYS_LD   : natural		  := WAYS_LD;
		ADDR_MASK : mem_address_type := (others => '1')
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		mem_out_cpu : in  mem_out_type;
		mem_in_cpu  : out mem_in_type;
		mem_out_mem : out mem_out_type;
		mem_in_mem  : in  mem_in_type
	);
end entity;

architecture impl of cache is --for testing
	alias cpu_to_cache : mem_out_type	is mem_out_cpu;
	alias cache_to_cpu : mem_in_type	is mem_in_cpu;
	alias cache_to_mem : mem_out_type	is mem_out_mem;
	alias mem_to_cache : mem_in_type	is mem_in_mem;

	signal cache_to_cpu_next : mem_in_type;


	type state_t is (IDLE, READ_CACHE, READ_MEM, WRITE_BACK);

	signal state : state_t := IDLE;
	signal state_next : state_t;


	signal rd_data_cache : std_logic;
	signal wr_data_cache : std_logic;
	signal rd_mgmt_cache : std_logic;
	signal wr_mgmt_cache : std_logic;


	signal valid_in : std_logic;
	signal valid_in_next : std_logic;
	signal dirty_in : std_logic;
	signal dirty_in_next : std_logic;


	alias tag   : c_tag_type   is cpu_to_cache.address(ADDR_WIDTH-1 downto INDEX_SIZE);
	alias index : c_index_type is cpu_to_cache.address(INDEX_SIZE-1 downto 0);

	signal valid_out  : std_logic;
	signal dirty_out  : std_logic;
	signal tag_out	: c_tag_type;
	signal hit_out	: std_logic;
	signal data_out   : mem_data_type;
	signal data_in	: mem_data_type;
	signal byteena_in : mem_byteena_type;

begin
	-- cache_to_mem<=cpu_to_cache;
	-- cache_to_cpu<=mem_to_cache;


	mgmt_st_inst : entity work.mgmt_st
		port map(
			clk			=> clk, -- in
			reset		=> reset, -- in

			index		=> index, -- in
			wr			=> wr_mgmt_cache, -- in
			rd			=> rd_mgmt_cache, -- in
			valid_in	=> valid_in, -- in
			dirty_in	=> dirty_in, -- in
			tag_in		=> tag, -- in
			-- way_out		=> open, -- out
			valid_out	=> valid_out, -- out
			dirty_out	=> dirty_out, -- out
			tag_out		=> tag_out, -- out
			hit_out		=> hit_out -- out
		);


	data_st_inst : entity work.data_st
		port map(
			clk			=> clk, -- in
			we			=> wr_data_cache, -- in
			rd			=> rd_data_cache, -- in
			-- way			=> (others => '0'), -- in
			index		=> index, -- in
			byteena		=> byteena_in, -- in
			data_in		=> data_in, -- in
			data_out	=> data_out -- out
		);


	sync : process(clk, reset)
	begin
		if (reset = '0') then
			state <= IDLE;
			cache_to_cpu <= MEM_IN_NOP;
		elsif (rising_edge(clk)) then
			state <= state_next;
			cache_to_cpu <= cache_to_cpu_next;
		end if;
	end process;


	nextout_logic : process(all)
	begin
		state_next <= state;
		cache_to_cpu_next <= cache_to_cpu;

		byteena_in <= (others => '0');

		valid_in <= '0';
		dirty_in <= '0';
		rd_data_cache <= '0';
		rd_mgmt_cache <= '0';
		wr_data_cache <= '0';
		wr_mgmt_cache <= '0';
		data_in <= (others => '0');
		cache_to_mem <= MEM_OUT_NOP;

		case state is
			when IDLE =>
				cache_to_cpu_next <= MEM_IN_NOP;
				-- 00 1111 1111 1111
				if (unsigned(ADDR_MASK) < unsigned(cpu_to_cache.address)) then
					-- bypass cache for device access (e.g. UART)
					cache_to_mem <= cpu_to_cache;
					cache_to_cpu_next <= mem_to_cache;

				else
					if cpu_to_cache.rd = '1' then
						rd_mgmt_cache		<= '1';
						rd_data_cache		<= '1';
						cache_to_cpu_next	<= (busy => '1', rddata => (others => '0'));
						state_next			<= READ_CACHE;
					end if;

					if cpu_to_cache.wr = '1' then
						rd_mgmt_cache		<= '1';
						if hit_out = '1' then
							-- mgmt
							wr_mgmt_cache	<= '1';
							valid_in		<= '1';
							dirty_in		<= '1';
							-- data
							wr_data_cache	<= '1';
							byteena_in		<= cpu_to_cache.byteena;
							data_in			<= cpu_to_cache.wrdata;
						else
							cache_to_mem	<= cpu_to_cache; -- addr, rd, byteena, wrdata
							cache_to_mem.wr	<= '1';
						end if;
					end if;
				end if;



			when READ_CACHE =>
				-- cache_to_cpu_next	<= (busy => '1', rddata => (others => '0'));
				rd_mgmt_cache		<= '1';

				-- hit = '1' ==> addr in cache
				if (hit_out = '1') then
					cache_to_cpu_next <= (busy => '0', rddata => data_out);
					state_next <= IDLE;


				-- hit = '0'
				else
					-- valid = '1' and dirty = '1'
					if (valid_out = '1' and dirty_out = '1') then
						-- wirte to mem and read the according addr
						-- cache_to_mem.address <= tag_out & index;
						cache_to_mem.address <= tag_out & index;
						cache_to_mem.wrdata <= data_out;
						cache_to_mem.wr <= '1';
						-- use default of MEM_OUT_NOP for rd, byteena

						rd_data_cache <= '1';
						rd_mgmt_cache <= '1';

						state_next <= WRITE_BACK;

					-- else (write over)
					else
						-- just read the according addr
						cache_to_mem.address <= cpu_to_cache.address;
						cache_to_mem.rd <= '1';
						-- use default of MEM_OUT_NOP for wr, byteena, wrdata

						state_next <= READ_MEM;

					end if;
				end if;



			when WRITE_BACK =>
				cache_to_mem.address	<= tag_out & index;
				cache_to_mem.wrdata		<= data_out;
				-- use default of MEM_OUT_NOP for rd, wr, byteena

				rd_data_cache <= '1';
				rd_mgmt_cache <= '1';

				if mem_to_cache.busy = '0' then
					-- just read the according addr
					cache_to_mem.address <= cpu_to_cache.address;
					cache_to_mem.rd <= '1';
					-- use default of MEM_OUT_NOP for wr, byteena, wrdata

					state_next <= READ_MEM;
				end if;



			when READ_MEM =>
				cache_to_mem.address <= cpu_to_cache.address;
				-- use default of MEM_OUT_NOP for rd, wr, byteena, wrdata

				if mem_to_cache.busy = '0' then
					-- write the data from mem to cache
					data_in			<= mem_to_cache.rddata;
					byteena_in		<= (others => '1');
					wr_data_cache	<= '1';

					-- update mgmt information
					valid_in		<= '1';
					dirty_in		<= '0';
					wr_mgmt_cache	<= '1';

					cache_to_cpu_next <= mem_to_cache;
					state_next <= IDLE;
				end if;
		end case;
	end process;

end architecture;
