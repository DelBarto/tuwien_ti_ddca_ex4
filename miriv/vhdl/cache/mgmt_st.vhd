library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.mem_pkg.all;
use work.cache_pkg.all;

entity mgmt_st is
    generic (
        SETS_LD  : natural := SETS_LD --;
        -- WAYS_LD  : natural := WAYS_LD
    );
    port (
        clk     : in std_logic;
        reset   : in std_logic;

        index   : in c_index_type;
        wr      : in std_logic;
        rd      : in std_logic;

        valid_in   : in std_logic;
        dirty_in   : in std_logic;
        tag_in     : in c_tag_type;
        -- way_out    : out c_way_type;
        valid_out  : out std_logic;
        dirty_out  : out std_logic;
        tag_out    : out c_tag_type;
        hit_out    : out std_logic
);
end entity;

architecture impl of mgmt_st is
    constant empty_mgmt_info : c_mgmt_info := (valid => '0', dirty => '0',
                                                -- replace => '0',
                                                tag => (others=> '0'));
    signal mgmt_info_in : c_mgmt_info;
    signal mgmt_info_out : c_mgmt_info;
begin

    mgmt_info_in.valid <= valid_in;
    mgmt_info_in.dirty <= dirty_in;
    -- mgmt_info_in.replace <= '0';
    mgmt_info_in.tag <= tag_in;

    mgmt_st_1w_inst : entity work.mgmt_st_1w
		generic map(
			SETS_LD => SETS_LD
		)
        port map(
            clk             => clk,
            reset           => reset,
            index           => index,
            we              => wr,
            -- we_repl         => '0',
            mgmt_info_in    => mgmt_info_in,
            mgmt_info_out   => mgmt_info_out
        );

    beh : process(all)
    begin
        if rd = '1' then
            valid_out <= mgmt_info_out.valid;
            dirty_out <= mgmt_info_out.dirty;
            tag_out   <= mgmt_info_out.tag;
				if ((tag_in = mgmt_info_out.tag) and (mgmt_info_out.valid = '1')) then
					hit_out <= '1';
				else
					hit_out <= '0';
				end if;

				--hit_out   <= '1' when ((tag_in = mgmt_info_out.tag) and (mgmt_info_out.valid = '0')) else '0';
        else
            valid_out <= empty_mgmt_info.valid;
            dirty_out <= empty_mgmt_info.dirty;
            tag_out   <= empty_mgmt_info.tag;
            hit_out   <= '0';
        end if;
    end process;

end architecture;
