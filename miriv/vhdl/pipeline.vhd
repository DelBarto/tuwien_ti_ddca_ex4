library ieee;
use ieee.std_logic_1164.all;

use work.core_pkg.all;
use work.mem_pkg.all;
use work.op_pkg.all;

entity pipeline is
	port (
		clk			: in  std_logic;
		reset		: in  std_logic;

		-- instruction interface
		mem_i_out	: out mem_out_type;
		mem_i_in	: in  mem_in_type;

		-- data interface
		mem_d_out	: out mem_out_type;
		mem_d_in	: in  mem_in_type
	);
end pipeline;

architecture impl of pipeline is

	-- output of fetch stage
	signal fet_mem_busy : std_logic;
	signal fet_pc_out : pc_type;
	signal fet_instr : instr_type;

	-- output of decode stage
	signal dec_pc_out : pc_type;
	signal dec_exec_op : exec_op_type;
	signal dec_mem_op : mem_op_type;
	signal dec_wb_op : wb_op_type;
	-- signal dec_exc_dec : std_logic;

	-- output of exec stage
	signal exec_pc_old_out : pc_type;
	signal exec_pc_new_out : pc_type;
	signal exec_aluresult : data_type;
	signal exec_wrdata : data_type;
	signal exec_zero : std_logic;
	signal exec_mem_op : mem_op_type;
	signal exec_wb_op : wb_op_type;

	-- output of mem stage
	signal mem_mem_busy			 : std_logic;
	signal mem_reg_write		 : reg_write_type;
	signal mem_pc_new_out		 : pc_type;
	signal mem_pcsrc			 : std_logic;
	signal mem_wbop_out			 : wb_op_type;
	signal mem_pc_old_out		 : pc_type;
	signal mem_aluresult_out	 : data_type;
	signal mem_memresult		 : data_type;

	-- output of wb stage
	signal wb_reg_write : reg_write_type;

	-- others signals
	signal stall : std_logic := '0';


	signal ctrl_stall_fetch	: std_logic;
	signal ctrl_stall_dec	: std_logic;
	signal ctrl_stall_exec	: std_logic;
	signal ctrl_stall_mem	: std_logic;
	signal ctrl_stall_wb	: std_logic;
	signal ctrl_flush_fetch	: std_logic;
	signal ctrl_flush_dec	: std_logic;
	signal ctrl_flush_exec	: std_logic;
	signal ctrl_flush_mem	: std_logic;
	signal ctrl_flush_wb	: std_logic;

	signal ctrl_pcsrc		: std_logic;

begin

	piping : process (reset, fet_mem_busy, mem_mem_busy)
	begin
		if reset = '0' then
			stall <= '0';
		else
			if fet_mem_busy = '1' or mem_mem_busy = '1' then -- memory busy
				stall <= '1';
			else
				stall <= '0';
			end if;
		end if;
	end process piping;


	fetch_inst : entity work.fetch
		port map(
			clk			=> clk,
			reset		=> reset,
			stall		=> ctrl_stall_fetch,
			flush		=> ctrl_flush_fetch,

			-- to control
			mem_busy	=> fet_mem_busy, --out

			pcsrc		=> ctrl_pcsrc, --mem_pcsrc,
			pc_in		=> mem_pc_new_out,
			pc_out		=> fet_pc_out, --out
			instr		=> fet_instr, --out

			-- memory controller interface
			mem_in		=> mem_i_in,
			mem_out		=> mem_i_out --out
		);


	decode_inst : entity work.decode
		port map(
			clk			=> clk,
			reset		=> reset,
			stall		=> ctrl_stall_dec,
			flush		=> ctrl_flush_dec,

			-- from fetch
			pc_in		=> fet_pc_out,
			instr		=> fet_instr,

			-- from writeback
			reg_write	=> wb_reg_write,

			-- towards next stages
			pc_out		=> dec_pc_out, --out
			exec_op		=> dec_exec_op, --out
			mem_op		=> dec_mem_op, --out
			wb_op		=> dec_wb_op, --out

			-- exceptions
			exc_dec		=> open --dec_exc_dec --out
		);

	exec_inst : entity work.exec
		port map(
			clk				=> clk,
			reset			=> reset,
			stall			=> ctrl_stall_exec,
			flush			=> ctrl_flush_exec,

			-- from DEC
			op				=> dec_exec_op,
			pc_in			=> dec_pc_out,

			-- to MEM
			pc_old_out		=> exec_pc_old_out,	--out
			pc_new_out		=> exec_pc_new_out,	--out
			aluresult		=> exec_aluresult,	--out
			wrdata			=> exec_wrdata,		--out
			zero			=> exec_zero,		--out

			memop_in		=> dec_mem_op,
			memop_out		=> exec_mem_op,		--out
			wbop_in			=> dec_wb_op,
			wbop_out		=> exec_wb_op,		--out

			-- to ctrl
			exec_op			=> open,	--out
			-- FWD
			reg_write_mem	=> mem_reg_write,
			reg_write_wr	=> wb_reg_write
		);

	mem_inst : entity work.mem
		port map(
			clk				=> clk,
			reset			=> reset,
			stall			=> ctrl_stall_mem,
			flush			=> ctrl_flush_mem,

			-- to Ctrl
			mem_busy		=> mem_mem_busy, --out

			-- from EXEC
			mem_op			=> exec_mem_op,
			wbop_in	  		=> exec_wb_op,
			pc_new_in		=> exec_pc_new_out,
			pc_old_in		=> exec_pc_old_out,
			aluresult_in	=> exec_aluresult,
			wrdata			=> exec_wrdata,
			zero			=> exec_zero,

			-- to EXEC (forwarding)
			reg_write		=> mem_reg_write, --out

			-- to FETCH
			pc_new_out		=> mem_pc_new_out, --out
			pcsrc			=> mem_pcsrc, --out

			-- to WB
			wbop_out		=> mem_wbop_out, --out
			pc_old_out		=> mem_pc_old_out, --out
			aluresult_out	=> mem_aluresult_out, --out
			memresult		=> mem_memresult, --out

			-- memory controller interface
			mem_in			=> mem_d_in,
			mem_out	 		=> mem_d_out, --out

			-- exceptions
			exc_load		=> open, --mem_exc_load, --out
			exc_store		=> open --mem_exc_store --out
		);

	wb_inst : entity work.wb
		port map(
			clk			=> clk,
			reset		=> reset,
			stall		=> ctrl_stall_wb,
			flush		=> ctrl_flush_wb,

			-- from MEM
			op			=> mem_wbop_out,
			aluresult	=> mem_aluresult_out,
			memresult	=> mem_memresult,
			pc_old_in	=> mem_pc_old_out,

			-- to FWD and DEC
			reg_write	=> wb_reg_write --out
		);





	-- crtl instants to control branch hazards
	ctrl_inst : entity work.ctrl
		port map(
			clk			=> clk,
			reset		=> reset,
			stall		=> stall,

			stall_fetch	=> ctrl_stall_fetch,	--out
			stall_dec	=> ctrl_stall_dec,		--out
			stall_exec	=> ctrl_stall_exec,		--out
			stall_mem	=> ctrl_stall_mem,		--out
			stall_wb	=> ctrl_stall_wb,		--out

			flush_fetch	=> ctrl_flush_fetch,	--out
			flush_dec	=> ctrl_flush_dec,		--out
			flush_exec	=> ctrl_flush_exec,		--out
			flush_mem	=> ctrl_flush_mem,		--out
			flush_wb	=> ctrl_flush_wb,		--out

			-- from FWD
			-- wb_op_mem   => mem_wbop_out,
			wb_op_mem   => exec_wb_op,
			exec_op     => dec_exec_op,

			pcsrc_in	=> mem_pcsrc,
			pcsrc_out	=> ctrl_pcsrc --out
		);


end architecture;
