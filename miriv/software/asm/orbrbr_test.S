		.text
		.align  2
		.globl  _start

_start:
		addi x1, x0, 1
		addi x2, x0, 1
		addi x3, x0, 1
		addi x5, x0, 10
		addi x6, x0, 20
		nop
		nop
loop:
		addi x5, x5, 1
		nop
		nop
		sw x5, 16(x0)
		nop
		nop
		nop
		add x7, x5, x6
		nop
		nop
		nop
		BNE x5, x6, loop
		nop
		nop
		nop
endl:
		nop
		nop
		nop
		jal x0, endl
		nop
		nop
		nop

		.end _start
		.size _start, .-_start
