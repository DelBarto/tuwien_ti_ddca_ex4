		.text
		.align  2
		.globl  _start

_start:
		addi x0, x0, 0
		lui x1, 4077
		addi x1, x1, 1629
		addi x1, x1, 1629
		sw	x1, 16(x0)
		nop
		lw	x3, 16(x0)
		lh	x4, 16(x0)
		lb	x5, 16(x0)
		lbu	x6, 16(x0)
		nop

		nop
loop:	j	loop
		nop
		nop
		nop

		.end _start
		.size _start, .-_start
