		.text
		.align  2
		.globl  _start

_start:
		addi x1, x0, 1
		addi x3, x0, 3
		addi x5, x0, 0
		addi x6, x0, 6
		addi x6, x0, 9
loop1:
		addi x5, x5, 1
		BNE x5, x6, loop1
loop2:
		sub x5, x5, x1 
		BGE x5, x3, loop2
		nop
endl:
		nop
		nop
		nop
		jal x0, endl
		nop
		nop
		nop

		.end _start
		.size _start, .-_start

