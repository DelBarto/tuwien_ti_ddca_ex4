#include "util.h"

void calccumsum(int *x,int *cumsum, int size){
	int i;
	for(i = 0; i < size; i++){
		x[i]=i;
		cumsum[i] = i;
		if (i > 0){
			cumsum[i] += cumsum[i-1];
		}
		
	}
}

int main() {
        int i;
	int size = 10;
	int x[size];
	int cumsum[size];
	
	calccumsum(x, cumsum, size);


	
	for(i = 0; i < size; i++){
		putchar('0'+cumsum[i]);
		putchar('\n');
	}



        return 0;
}

