
cumsum.elf:     file format elf32-littleriscv


Disassembly of section .text:

40000000 <_start>:
40000000:	00000013          	addi	zero,zero,0
40000004:	c0001197          	auipc	gp,0xc0001
40000008:	80018193          	addi	gp,gp,-2048 # 804 <__global_pointer$>
4000000c:	00001137          	lui	sp,0x1
40000010:	018000ef          	jal	ra,40000028 <main>
40000014:	00000013          	addi	zero,zero,0

40000018 <loop>:
40000018:	0000006f          	jal	zero,40000018 <loop>
4000001c:	00000013          	addi	zero,zero,0
40000020:	00000013          	addi	zero,zero,0
40000024:	00000013          	addi	zero,zero,0

40000028 <main>:
40000028:	fa010113          	addi	sp,sp,-96 # fa0 <__global_pointer$+0x79c>
4000002c:	00a00613          	addi	a2,zero,10
40000030:	02810593          	addi	a1,sp,40
40000034:	00010513          	addi	a0,sp,0
40000038:	04812c23          	sw	s0,88(sp)
4000003c:	04912a23          	sw	s1,84(sp)
40000040:	04112e23          	sw	ra,92(sp)
40000044:	02810413          	addi	s0,sp,40
40000048:	03c000ef          	jal	ra,40000084 <calccumsum>
4000004c:	05010493          	addi	s1,sp,80
40000050:	00042503          	lw	a0,0(s0)
40000054:	00440413          	addi	s0,s0,4
40000058:	03050513          	addi	a0,a0,48
4000005c:	080000ef          	jal	ra,400000dc <putchar>
40000060:	00a00513          	addi	a0,zero,10
40000064:	078000ef          	jal	ra,400000dc <putchar>
40000068:	fe9414e3          	bne	s0,s1,40000050 <main+0x28>
4000006c:	05c12083          	lw	ra,92(sp)
40000070:	05812403          	lw	s0,88(sp)
40000074:	05412483          	lw	s1,84(sp)
40000078:	00000513          	addi	a0,zero,0
4000007c:	06010113          	addi	sp,sp,96
40000080:	00008067          	jalr	zero,0(ra)

40000084 <calccumsum>:
40000084:	02c05e63          	bge	zero,a2,400000c0 <calccumsum+0x3c>
40000088:	00052023          	sw	zero,0(a0)
4000008c:	0005a023          	sw	zero,0(a1)
40000090:	00450513          	addi	a0,a0,4
40000094:	00000793          	addi	a5,zero,0
40000098:	0180006f          	jal	zero,400000b0 <calccumsum+0x2c>
4000009c:	00f52023          	sw	a5,0(a0)
400000a0:	ffc5a703          	lw	a4,-4(a1)
400000a4:	00450513          	addi	a0,a0,4
400000a8:	00f70733          	add	a4,a4,a5
400000ac:	00e5a023          	sw	a4,0(a1)
400000b0:	00178793          	addi	a5,a5,1
400000b4:	00458593          	addi	a1,a1,4
400000b8:	fef612e3          	bne	a2,a5,4000009c <calccumsum+0x18>
400000bc:	00008067          	jalr	zero,0(ra)
400000c0:	00008067          	jalr	zero,0(ra)

400000c4 <getchar>:
400000c4:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
400000c8:	0027f793          	andi	a5,a5,2
400000cc:	fe078ce3          	beq	a5,zero,400000c4 <getchar>
400000d0:	ffc04503          	lbu	a0,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
400000d4:	0ff57513          	andi	a0,a0,255
400000d8:	00008067          	jalr	zero,0(ra)

400000dc <putchar>:
400000dc:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
400000e0:	0017f793          	andi	a5,a5,1
400000e4:	fe078ce3          	beq	a5,zero,400000dc <putchar>
400000e8:	0ff57793          	andi	a5,a0,255
400000ec:	fef00e23          	sb	a5,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
400000f0:	00008067          	jalr	zero,0(ra)

400000f4 <time>:
400000f4:	ffff87b7          	lui	a5,0xffff8
400000f8:	0007a503          	lw	a0,0(a5) # ffff8000 <__modsi3+0xbfff7cb8>
400000fc:	00008067          	jalr	zero,0(ra)

40000100 <putnumber>:
40000100:	fd010113          	addi	sp,sp,-48
40000104:	01312e23          	sw	s3,28(sp)
40000108:	01512a23          	sw	s5,20(sp)
4000010c:	00410993          	addi	s3,sp,4
40000110:	00a00a93          	addi	s5,zero,10
40000114:	02912223          	sw	s1,36(sp)
40000118:	03212023          	sw	s2,32(sp)
4000011c:	01612823          	sw	s6,16(sp)
40000120:	02112623          	sw	ra,44(sp)
40000124:	02812423          	sw	s0,40(sp)
40000128:	01412c23          	sw	s4,24(sp)
4000012c:	00050493          	addi	s1,a0,0
40000130:	000107a3          	sb	zero,15(sp)
40000134:	00098913          	addi	s2,s3,0
40000138:	413a8ab3          	sub	s5,s5,s3
4000013c:	00900b13          	addi	s6,zero,9
40000140:	00a00593          	addi	a1,zero,10
40000144:	00048513          	addi	a0,s1,0
40000148:	1cc000ef          	jal	ra,40000314 <__umodsi3>
4000014c:	03050413          	addi	s0,a0,48
40000150:	0ff47413          	andi	s0,s0,255
40000154:	00048513          	addi	a0,s1,0
40000158:	00890523          	sb	s0,10(s2)
4000015c:	00a00593          	addi	a1,zero,10
40000160:	16c000ef          	jal	ra,400002cc <__udivsi3>
40000164:	00048a13          	addi	s4,s1,0
40000168:	012a8733          	add	a4,s5,s2
4000016c:	00050493          	addi	s1,a0,0
40000170:	fff90913          	addi	s2,s2,-1
40000174:	fd4b66e3          	bltu	s6,s4,40000140 <putnumber+0x40>
40000178:	00e98733          	add	a4,s3,a4
4000017c:	00170713          	addi	a4,a4,1
40000180:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
40000184:	0017f793          	andi	a5,a5,1
40000188:	fe078ce3          	beq	a5,zero,40000180 <putnumber+0x80>
4000018c:	fe800e23          	sb	s0,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
40000190:	00074403          	lbu	s0,0(a4)
40000194:	fe0414e3          	bne	s0,zero,4000017c <putnumber+0x7c>
40000198:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
4000019c:	0017f793          	andi	a5,a5,1
400001a0:	fe078ce3          	beq	a5,zero,40000198 <putnumber+0x98>
400001a4:	00a00793          	addi	a5,zero,10
400001a8:	fef00e23          	sb	a5,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
400001ac:	02c12083          	lw	ra,44(sp)
400001b0:	02812403          	lw	s0,40(sp)
400001b4:	02412483          	lw	s1,36(sp)
400001b8:	02012903          	lw	s2,32(sp)
400001bc:	01c12983          	lw	s3,28(sp)
400001c0:	01812a03          	lw	s4,24(sp)
400001c4:	01412a83          	lw	s5,20(sp)
400001c8:	01012b03          	lw	s6,16(sp)
400001cc:	03010113          	addi	sp,sp,48
400001d0:	00008067          	jalr	zero,0(ra)

400001d4 <putstring>:
400001d4:	00054703          	lbu	a4,0(a0)
400001d8:	02070063          	beq	a4,zero,400001f8 <putstring+0x24>
400001dc:	00150513          	addi	a0,a0,1
400001e0:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
400001e4:	0017f793          	andi	a5,a5,1
400001e8:	fe078ce3          	beq	a5,zero,400001e0 <putstring+0xc>
400001ec:	fee00e23          	sb	a4,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
400001f0:	00054703          	lbu	a4,0(a0)
400001f4:	fe0714e3          	bne	a4,zero,400001dc <putstring+0x8>
400001f8:	00000513          	addi	a0,zero,0
400001fc:	00008067          	jalr	zero,0(ra)

40000200 <puts>:
40000200:	00054703          	lbu	a4,0(a0)
40000204:	02070063          	beq	a4,zero,40000224 <puts+0x24>
40000208:	00150513          	addi	a0,a0,1
4000020c:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
40000210:	0017f793          	andi	a5,a5,1
40000214:	fe078ce3          	beq	a5,zero,4000020c <puts+0xc>
40000218:	fee00e23          	sb	a4,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
4000021c:	00054703          	lbu	a4,0(a0)
40000220:	fe0714e3          	bne	a4,zero,40000208 <puts+0x8>
40000224:	ff804783          	lbu	a5,-8(zero) # fffffff8 <__modsi3+0xbffffcb0>
40000228:	0017f793          	andi	a5,a5,1
4000022c:	fe078ce3          	beq	a5,zero,40000224 <puts+0x24>
40000230:	00a00793          	addi	a5,zero,10
40000234:	fef00e23          	sb	a5,-4(zero) # fffffffc <__modsi3+0xbffffcb4>
40000238:	00000513          	addi	a0,zero,0
4000023c:	00008067          	jalr	zero,0(ra)

40000240 <memcpy>:
40000240:	00050793          	addi	a5,a0,0
40000244:	02060063          	beq	a2,zero,40000264 <memcpy+0x24>
40000248:	00c50533          	add	a0,a0,a2
4000024c:	0005c703          	lbu	a4,0(a1)
40000250:	00178793          	addi	a5,a5,1
40000254:	00158593          	addi	a1,a1,1
40000258:	fee78fa3          	sb	a4,-1(a5)
4000025c:	fef518e3          	bne	a0,a5,4000024c <memcpy+0xc>
40000260:	00008067          	jalr	zero,0(ra)
40000264:	00008067          	jalr	zero,0(ra)

40000268 <strlen>:
40000268:	00054783          	lbu	a5,0(a0)
4000026c:	00050713          	addi	a4,a0,0
40000270:	00000513          	addi	a0,zero,0
40000274:	00078c63          	beq	a5,zero,4000028c <strlen+0x24>
40000278:	00150513          	addi	a0,a0,1
4000027c:	00a707b3          	add	a5,a4,a0
40000280:	0007c783          	lbu	a5,0(a5)
40000284:	fe079ae3          	bne	a5,zero,40000278 <strlen+0x10>
40000288:	00008067          	jalr	zero,0(ra)
4000028c:	00008067          	jalr	zero,0(ra)

40000290 <strcmp>:
40000290:	00050693          	addi	a3,a0,0
40000294:	00c0006f          	jal	zero,400002a0 <strcmp+0x10>
40000298:	40f70533          	sub	a0,a4,a5
4000029c:	02f71263          	bne	a4,a5,400002c0 <strcmp+0x30>
400002a0:	0006c703          	lbu	a4,0(a3)
400002a4:	0005c783          	lbu	a5,0(a1)
400002a8:	00168693          	addi	a3,a3,1
400002ac:	00158593          	addi	a1,a1,1
400002b0:	00f76633          	or	a2,a4,a5
400002b4:	fe0612e3          	bne	a2,zero,40000298 <strcmp+0x8>
400002b8:	00000513          	addi	a0,zero,0
400002bc:	00008067          	jalr	zero,0(ra)
400002c0:	00008067          	jalr	zero,0(ra)

400002c4 <__divsi3>:
400002c4:	06054063          	blt	a0,zero,40000324 <__umodsi3+0x10>
400002c8:	0605c663          	blt	a1,zero,40000334 <__umodsi3+0x20>

400002cc <__udivsi3>:
400002cc:	00058613          	addi	a2,a1,0
400002d0:	00050593          	addi	a1,a0,0
400002d4:	fff00513          	addi	a0,zero,-1
400002d8:	02060c63          	beq	a2,zero,40000310 <__udivsi3+0x44>
400002dc:	00100693          	addi	a3,zero,1
400002e0:	00b67a63          	bgeu	a2,a1,400002f4 <__udivsi3+0x28>
400002e4:	00c05863          	bge	zero,a2,400002f4 <__udivsi3+0x28>
400002e8:	00161613          	slli	a2,a2,0x1
400002ec:	00169693          	slli	a3,a3,0x1
400002f0:	feb66ae3          	bltu	a2,a1,400002e4 <__udivsi3+0x18>
400002f4:	00000513          	addi	a0,zero,0
400002f8:	00c5e663          	bltu	a1,a2,40000304 <__udivsi3+0x38>
400002fc:	40c585b3          	sub	a1,a1,a2
40000300:	00d56533          	or	a0,a0,a3
40000304:	0016d693          	srli	a3,a3,0x1
40000308:	00165613          	srli	a2,a2,0x1
4000030c:	fe0696e3          	bne	a3,zero,400002f8 <__udivsi3+0x2c>
40000310:	00008067          	jalr	zero,0(ra)

40000314 <__umodsi3>:
40000314:	00008293          	addi	t0,ra,0
40000318:	fb5ff0ef          	jal	ra,400002cc <__udivsi3>
4000031c:	00058513          	addi	a0,a1,0
40000320:	00028067          	jalr	zero,0(t0)
40000324:	40a00533          	sub	a0,zero,a0
40000328:	00b04863          	blt	zero,a1,40000338 <__umodsi3+0x24>
4000032c:	40b005b3          	sub	a1,zero,a1
40000330:	f9dff06f          	jal	zero,400002cc <__udivsi3>
40000334:	40b005b3          	sub	a1,zero,a1
40000338:	00008293          	addi	t0,ra,0
4000033c:	f91ff0ef          	jal	ra,400002cc <__udivsi3>
40000340:	40a00533          	sub	a0,zero,a0
40000344:	00028067          	jalr	zero,0(t0)

40000348 <__modsi3>:
40000348:	00008293          	addi	t0,ra,0
4000034c:	0005ca63          	blt	a1,zero,40000360 <__modsi3+0x18>
40000350:	00054c63          	blt	a0,zero,40000368 <__modsi3+0x20>
40000354:	f79ff0ef          	jal	ra,400002cc <__udivsi3>
40000358:	00058513          	addi	a0,a1,0
4000035c:	00028067          	jalr	zero,0(t0)
40000360:	40b005b3          	sub	a1,zero,a1
40000364:	fe0558e3          	bge	a0,zero,40000354 <__modsi3+0xc>
40000368:	40a00533          	sub	a0,zero,a0
4000036c:	f61ff0ef          	jal	ra,400002cc <__udivsi3>
40000370:	40b00533          	sub	a0,zero,a1
40000374:	00028067          	jalr	zero,0(t0)

Disassembly of section .comment:

00000000 <.comment>:
   0:	3a434347          	fmsub.d	ft6,ft6,ft4,ft7,rmm
   4:	2820                	c.fld	fs0,80(s0)
   6:	29554e47          	fmsub.s	ft8,fa0,fs5,ft5,rmm
   a:	3120                	c.fld	fs0,96(a0)
   c:	2e30                	c.fld	fa2,88(a2)
   e:	2e31                	c.jal	32a <__BSS_END__+0x326>
  10:	0030                	c.addi4spn	a2,sp,8

Disassembly of section .riscv.attributes:

00000000 <.riscv.attributes>:
   0:	1b41                	c.addi	s6,-16
   2:	0000                	c.unimp
   4:	7200                	c.flw	fs0,32(a2)
   6:	7369                	c.lui	t1,0xffffa
   8:	01007663          	bgeu	zero,a6,14 <__BSS_END__+0x10>
   c:	0011                	c.addi	zero,4
   e:	0000                	c.unimp
  10:	1004                	c.addi4spn	s1,sp,32
  12:	7205                	c.lui	tp,0xfffe1
  14:	3376                	c.fldsp	ft6,376(sp)
  16:	6932                	c.flwsp	fs2,12(sp)
  18:	7032                	c.flwsp	ft0,44(sp)
  1a:	0030                	c.addi4spn	a2,sp,8

Disassembly of section .debug_aranges:

00000000 <.debug_aranges>:
   0:	001c                	0x1c
   2:	0000                	c.unimp
   4:	0002                	c.slli64	zero
   6:	0000                	c.unimp
   8:	0000                	c.unimp
   a:	0004                	0x4
   c:	0000                	c.unimp
   e:	0000                	c.unimp
  10:	02c4                	c.addi4spn	s1,sp,324
  12:	4000                	c.lw	s0,0(s0)
  14:	00b4                	c.addi4spn	a3,sp,72
	...

Disassembly of section .debug_info:

00000000 <.debug_info>:
   0:	0022                	c.slli	zero,0x8
   2:	0000                	c.unimp
   4:	0002                	c.slli64	zero
   6:	0000                	c.unimp
   8:	0000                	c.unimp
   a:	0104                	c.addi4spn	s1,sp,128
   c:	0000                	c.unimp
   e:	0000                	c.unimp
  10:	02c4                	c.addi4spn	s1,sp,324
  12:	4000                	c.lw	s0,0(s0)
  14:	0378                	c.addi4spn	a4,sp,396
  16:	4000                	c.lw	s0,0(s0)
  18:	0000                	c.unimp
  1a:	0000                	c.unimp
  1c:	0030                	c.addi4spn	a2,sp,8
  1e:	0000                	c.unimp
  20:	008a                	c.slli	ra,0x2
  22:	0000                	c.unimp
  24:	8001                	c.srli64	s0

Disassembly of section .debug_abbrev:

00000000 <.debug_abbrev>:
   0:	1101                	c.addi	sp,-32
   2:	1000                	c.addi4spn	s0,sp,32
   4:	1106                	c.slli	sp,0x21
   6:	1201                	c.addi	tp,-32
   8:	0301                	c.addi	t1,0
   a:	1b0e                	c.slli	s6,0x23
   c:	250e                	c.fldsp	fa0,192(sp)
   e:	130e                	c.slli	t1,0x23
  10:	0005                	c.addi	zero,1
	...

Disassembly of section .debug_line:

00000000 <.debug_line>:
   0:	0165                	c.addi	sp,25
   2:	0000                	c.unimp
   4:	00460003          	lb	zero,4(a2)
   8:	0000                	c.unimp
   a:	0101                	c.addi	sp,0
   c:	000d0efb          	0xd0efb
  10:	0101                	c.addi	sp,0
  12:	0101                	c.addi	sp,0
  14:	0000                	c.unimp
  16:	0100                	c.addi4spn	s0,sp,128
  18:	0000                	c.unimp
  1a:	2e01                	c.jal	32a <__BSS_END__+0x326>
  1c:	2f2e                	c.fldsp	ft10,200(sp)
  1e:	2e2e                	c.fldsp	ft8,200(sp)
  20:	2f2e2e2f          	0x2f2e2e2f
  24:	2e2e                	c.fldsp	ft8,200(sp)
  26:	7369722f          	0x7369722f
  2a:	672d7663          	bgeu	s10,s2,696 <__BSS_END__+0x692>
  2e:	6c2f6363          	bltu	t5,sp,6f4 <__BSS_END__+0x6f0>
  32:	6269                	c.lui	tp,0x1a
  34:	2f636367          	0x2f636367
  38:	666e6f63          	bltu	t3,t1,6b6 <__BSS_END__+0x6b2>
  3c:	6769                	c.lui	a4,0x1a
  3e:	7369722f          	0x7369722f
  42:	00007663          	bgeu	zero,zero,4e <__BSS_END__+0x4a>
  46:	6964                	c.flw	fs1,84(a0)
  48:	2e76                	c.fldsp	ft8,344(sp)
  4a:	00010053          	fadd.s	ft0,ft2,ft0,rne
  4e:	0000                	c.unimp
  50:	0500                	c.addi4spn	s0,sp,640
  52:	c402                	c.swsp	zero,8(sp)
  54:	0002                	c.slli64	zero
  56:	0340                	c.addi4spn	s0,sp,388
  58:	00c4                	c.addi4spn	s1,sp,68
  5a:	0301                	c.addi	t1,0
  5c:	0901                	c.addi	s2,0
  5e:	0004                	0x4
  60:	0301                	c.addi	t1,0
  62:	0904                	c.addi4spn	s1,sp,144
  64:	0004                	0x4
  66:	0301                	c.addi	t1,0
  68:	0901                	c.addi	s2,0
  6a:	0004                	0x4
  6c:	0301                	c.addi	t1,0
  6e:	0901                	c.addi	s2,0
  70:	0004                	0x4
  72:	0301                	c.addi	t1,0
  74:	0901                	c.addi	s2,0
  76:	0004                	0x4
  78:	0301                	c.addi	t1,0
  7a:	0901                	c.addi	s2,0
  7c:	0004                	0x4
  7e:	0301                	c.addi	t1,0
  80:	0901                	c.addi	s2,0
  82:	0004                	0x4
  84:	0301                	c.addi	t1,0
  86:	0902                	c.slli64	s2
  88:	0004                	0x4
  8a:	0301                	c.addi	t1,0
  8c:	0901                	c.addi	s2,0
  8e:	0004                	0x4
  90:	0301                	c.addi	t1,0
  92:	0901                	c.addi	s2,0
  94:	0004                	0x4
  96:	0301                	c.addi	t1,0
  98:	0901                	c.addi	s2,0
  9a:	0004                	0x4
  9c:	0301                	c.addi	t1,0
  9e:	0902                	c.slli64	s2
  a0:	0004                	0x4
  a2:	0301                	c.addi	t1,0
  a4:	0902                	c.slli64	s2
  a6:	0004                	0x4
  a8:	0301                	c.addi	t1,0
  aa:	0901                	c.addi	s2,0
  ac:	0004                	0x4
  ae:	0301                	c.addi	t1,0
  b0:	0901                	c.addi	s2,0
  b2:	0004                	0x4
  b4:	0301                	c.addi	t1,0
  b6:	0902                	c.slli64	s2
  b8:	0004                	0x4
  ba:	0301                	c.addi	t1,0
  bc:	0901                	c.addi	s2,0
  be:	0004                	0x4
  c0:	0301                	c.addi	t1,0
  c2:	0901                	c.addi	s2,0
  c4:	0004                	0x4
  c6:	0301                	c.addi	t1,0
  c8:	0902                	c.slli64	s2
  ca:	0004                	0x4
  cc:	0301                	c.addi	t1,0
  ce:	0905                	c.addi	s2,1
  d0:	0004                	0x4
  d2:	0301                	c.addi	t1,0
  d4:	0901                	c.addi	s2,0
  d6:	0004                	0x4
  d8:	0301                	c.addi	t1,0
  da:	0901                	c.addi	s2,0
  dc:	0004                	0x4
  de:	0301                	c.addi	t1,0
  e0:	0901                	c.addi	s2,0
  e2:	0004                	0x4
  e4:	0301                	c.addi	t1,0
  e6:	0905                	c.addi	s2,1
  e8:	0004                	0x4
  ea:	0301                	c.addi	t1,0
  ec:	0902                	c.slli64	s2
  ee:	0004                	0x4
  f0:	0301                	c.addi	t1,0
  f2:	0902                	c.slli64	s2
  f4:	0004                	0x4
  f6:	0301                	c.addi	t1,0
  f8:	0901                	c.addi	s2,0
  fa:	0004                	0x4
  fc:	0301                	c.addi	t1,0
  fe:	0902                	c.slli64	s2
 100:	0004                	0x4
 102:	0301                	c.addi	t1,0
 104:	0902                	c.slli64	s2
 106:	0004                	0x4
 108:	0301                	c.addi	t1,0
 10a:	0901                	c.addi	s2,0
 10c:	0004                	0x4
 10e:	0301                	c.addi	t1,0
 110:	0901                	c.addi	s2,0
 112:	0004                	0x4
 114:	0301                	c.addi	t1,0
 116:	0901                	c.addi	s2,0
 118:	0004                	0x4
 11a:	0301                	c.addi	t1,0
 11c:	0904                	c.addi4spn	s1,sp,144
 11e:	0004                	0x4
 120:	0301                	c.addi	t1,0
 122:	0901                	c.addi	s2,0
 124:	0004                	0x4
 126:	0301                	c.addi	t1,0
 128:	0901                	c.addi	s2,0
 12a:	0004                	0x4
 12c:	0301                	c.addi	t1,0
 12e:	0902                	c.slli64	s2
 130:	0004                	0x4
 132:	0301                	c.addi	t1,0
 134:	0901                	c.addi	s2,0
 136:	0004                	0x4
 138:	0301                	c.addi	t1,0
 13a:	0901                	c.addi	s2,0
 13c:	0004                	0x4
 13e:	0301                	c.addi	t1,0
 140:	0902                	c.slli64	s2
 142:	0004                	0x4
 144:	0301                	c.addi	t1,0
 146:	0901                	c.addi	s2,0
 148:	0004                	0x4
 14a:	0301                	c.addi	t1,0
 14c:	0902                	c.slli64	s2
 14e:	0004                	0x4
 150:	0301                	c.addi	t1,0
 152:	0901                	c.addi	s2,0
 154:	0004                	0x4
 156:	0301                	c.addi	t1,0
 158:	0901                	c.addi	s2,0
 15a:	0004                	0x4
 15c:	0301                	c.addi	t1,0
 15e:	0901                	c.addi	s2,0
 160:	0004                	0x4
 162:	0901                	c.addi	s2,0
 164:	0004                	0x4
 166:	0100                	c.addi4spn	s0,sp,128
 168:	01              	Address 0x0000000000000168 is out of bounds.


Disassembly of section .debug_str:

00000000 <.debug_str>:
   0:	2e2e                	c.fldsp	ft8,200(sp)
   2:	2f2e2e2f          	0x2f2e2e2f
   6:	2e2e                	c.fldsp	ft8,200(sp)
   8:	2f2e2e2f          	0x2f2e2e2f
   c:	6972                	c.flwsp	fs2,28(sp)
   e:	2d766373          	csrrsi	t1,0x2d7,12
  12:	2f636367          	0x2f636367
  16:	696c                	c.flw	fa1,84(a0)
  18:	6762                	c.flwsp	fa4,24(sp)
  1a:	632f6363          	bltu	t5,s2,640 <__BSS_END__+0x63c>
  1e:	69666e6f          	jal	t3,666b4 <__global_pointer$+0x65eb0>
  22:	69722f67          	0x69722f67
  26:	2f766373          	csrrsi	t1,0x2f7,12
  2a:	6964                	c.flw	fs1,84(a0)
  2c:	2e76                	c.fldsp	ft8,344(sp)
  2e:	742f0053          	0x742f0053
  32:	706d                	c.lui	zero,0xffffb
  34:	7435722f          	0x7435722f
  38:	736c6f6f          	jal	t5,c676e <__global_pointer$+0xc5f6a>
  3c:	7369722f          	0x7369722f
  40:	672d7663          	bgeu	s10,s2,6ac <__BSS_END__+0x6a8>
  44:	756e                	c.flwsp	fa0,248(sp)
  46:	742d                	c.lui	s0,0xfffeb
  48:	636c6f6f          	jal	t5,c667e <__global_pointer$+0xc5e7a>
  4c:	6168                	c.flw	fa0,68(a0)
  4e:	6e69                	c.lui	t3,0x1a
  50:	6975622f          	0x6975622f
  54:	646c                	c.flw	fa1,76(s0)
  56:	6975622f          	0x6975622f
  5a:	646c                	c.flw	fa1,76(s0)
  5c:	672d                	c.lui	a4,0xb
  5e:	6e2d6363          	bltu	s10,sp,744 <__BSS_END__+0x740>
  62:	7765                	c.lui	a4,0xffff9
  64:	696c                	c.flw	fa1,84(a0)
  66:	2d62                	c.fldsp	fs10,24(sp)
  68:	67617473          	csrrci	s0,0x676,2
  6c:	3265                	c.jal	fffffa14 <__modsi3+0xbffff6cc>
  6e:	7369722f          	0x7369722f
  72:	32337663          	bgeu	t1,gp,39e <__BSS_END__+0x39a>
  76:	752d                	c.lui	a0,0xfffeb
  78:	6b6e                	c.flwsp	fs6,216(sp)
  7a:	6f6e                	c.flwsp	ft10,216(sp)
  7c:	652d6e77          	0x652d6e77
  80:	666c                	c.flw	fa1,76(a2)
  82:	62696c2f          	0x62696c2f
  86:	00636367          	0x636367
  8a:	20554e47          	fmsub.s	ft8,fa0,ft5,ft4,rmm
  8e:	5341                	c.li	t1,-16
  90:	3220                	c.fld	fs0,96(a2)
  92:	332e                	c.fldsp	ft6,232(sp)
  94:	0034                	c.addi4spn	a3,sp,8
