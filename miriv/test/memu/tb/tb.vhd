library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std; -- for Printing
use std.textio.all;

use work.mem_pkg.all;
use work.op_pkg.all;
use work.core_pkg.all;
use work.tb_util_pkg.all;

entity tb is
end entity;

architecture bench of tb is

	constant CLK_PERIOD : time := 10 ns;

	signal clk : std_logic;
	signal res_n : std_logic := '0';

	file input_file : text;
	file output_ref_file : text;

	type INPUT is
		record
			op	: memu_op_type;
			A	: data_type;
			W	: data_type;
			D	: mem_in_type;
		end record;

	type OUTPUT is
		record
			R	: data_type;
			B	: std_logic;
			XL	: std_logic;
			XS	: std_logic;
			M	: mem_out_type;
		end record;

	signal inp  : INPUT := (
		MEMU_NOP,
		(others => '0'),
		(others => '0'),
		MEM_IN_NOP
	);

	signal outp : OUTPUT;


	impure function read_next_input(file f : text) return INPUT is
		variable l : line;
		variable result : INPUT;
	begin
		l := get_next_valid_line(f);
		result.op.memtype := str_to_mem_op(l.all);

		l := get_next_valid_line(f);
		result.op.memread := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.op.memwrite := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.A := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.W := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.D := to_mem_in_type(bin_to_slv(l.all, DATA_WIDTH+1));

		return result;
	end function;

	impure function read_next_output(file f : text) return OUTPUT is
		variable l : line;
		variable result : OUTPUT;
	begin
		l := get_next_valid_line(f);
		result.R := bin_to_slv(l.all, DATA_WIDTH);

		l := get_next_valid_line(f);
		result.B := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.XL := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.XS := str_to_sl(l(1));

		l := get_next_valid_line(f);
		result.M := to_mem_out_type(bin_to_slv(l.all, DATA_WIDTH + ADDR_WIDTH + 1 + 1 + BYTEEN_WIDTH));

		return result;
	end function;

	procedure check_output(output_ref : OUTPUT) is
		variable passed : boolean;
		variable M_slv : std_logic_vector(mem_out_range_type'high downto 0);
		variable M_slv_ref : std_logic_vector(mem_out_range_type'high downto 0);
		variable D_slv : std_logic_vector(mem_in_range_type'high downto 0);
	begin
		passed := (outp = output_ref);

		D_slv := to_std_logic_vector(inp.D);

		if passed then
			report " PASSED: "
			& "op=" & to_string(inp.op.memtype)
			& " A=" & slv_to_bin(inp.A)
			& " W=" & slv_to_bin(inp.W)
			& " D=" & slv_to_bin(D_slv)
			& lf
			severity note;
		else
			M_slv := to_std_logic_vector(outp.M);
			M_slv_ref := to_std_logic_vector(output_ref.M);

			report "FAILED: "
			& "op=" & to_string(inp.op.memtype)
			& " A=" & slv_to_bin(inp.A)
			& " W=" & slv_to_bin(inp.W)
			& " D=" & slv_to_bin(D_slv)
			& lf
			& "**      expected: R=" & slv_to_bin(output_ref.R)
			& " B=" & to_string(output_ref.B)
			& " XL=" & to_string(output_ref.XL)
			& " XS=" & to_string(output_ref.XS)
			& " M=" & slv_to_bin(M_slv_ref)
			& lf
			& "**        actual: R=" & slv_to_bin(outp.R)
			& " B=" & to_string(outp.B)
			& " XL=" & to_string(outp.XL)
			& " XS=" & to_string(outp.XS)
			& " M=" & slv_to_bin(M_slv)
			& lf
			severity error;
		end if;
	end procedure;

begin

	memu_inst : entity work.memu
		port map
		(
			op	=> inp.op,
			A	=> inp.A,
			W	=> inp.W,
			D	=> inp.D,
			R	=> outp.R,
			B	=> outp.B,
			XL	=> outp.XL,
			XS	=> outp.XS,
			M	=> outp.M
		);

	stimulus : process
		variable fstatus: file_open_status;
	begin
		file_open(fstatus, input_file, "testdata/input.txt", READ_MODE);

		wait until res_n = '1';
		timeout(1, CLK_PERIOD);

		while not endfile(input_file) loop
			inp <= read_next_input(input_file);
			timeout(1, CLK_PERIOD);
		end loop;

		wait;
	end process;

	output_checker : process
		variable fstatus: file_open_status;
		variable output_ref : OUTPUT;
	begin
		file_open(fstatus, output_ref_file, "testdata/output.txt", READ_MODE);

		wait until res_n = '1';
		timeout(1, CLK_PERIOD);

		while not endfile(output_ref_file) loop
			output_ref := read_next_output(output_ref_file);

			wait until falling_edge(clk);
			check_output(output_ref);
			wait until rising_edge(clk);
		end loop;

		wait;
	end process;

	generate_clk : process
	begin
		clk_generate(clk, CLK_PERIOD);
		wait;
	end process;

	generate_reset : process
	begin
		res_n <= '0';
		wait until rising_edge(clk);
		res_n <= '1';
		wait;
	end process;

end architecture;
